import subprocess
import json

class Lbry:
      def __init__(self):
            self.blobs = []
            self.files = []
            self.daemon = None

      def run_command(self, command):
            try:
                  output = subprocess.check_output(command, shell=True)

                  output_json = json.loads(output)

                  return output_json, None
            except json.JSONDecodeError as e:
                  return False, e
            except OSError as e:
                  return False, e
            except subprocess.CalledProcessError as e:
                  return False, e


      def start_daemon(self):            
            if not self.is_running():
                  print("Starting daemon")
                  self.daemon = subprocess.Popen(["./lbrynet", "start"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                  return True
            return False

      def stop_daemon(self):
            if self.is_running():
                  return subprocess.check_output("./lbrynet stop", shell=True)

            return False

      def is_running(self):
            return self.get_status()["is_running"]

      def get_status(self):
            result, error = self.run_command("./lbrynet status")

            if error:
                  return {"is_running": result, "error": error}

            return result
      
      def get_uri(self, uri):
            result, error = self.run_command("./lbrynet get " + uri)

            if error:
                  return {"error": error}

            return result

      def get_blob_page(self, page, blobs):
            result, error = self.run_command("./lbrynet blob list --page_size=500 --page=" + str(page))

            if error:
                  blobs.append({"error": error, "page": page})

            for blob in result["items"]:
                  blobs.append(blob)

            if result["page"] < result["total_pages"]:
                  return self.get_blob_page(page + 1, blobs)

            return blobs

      def get_blobs(self):
            blobs = self.get_blob_page(1, [])

            safe_blobs = []

            for blob in blobs:
                  if "error" in blob:
                        print("Error")
                        print(blob["page"])
                        print(blob["error"])
                  else:
                        safe_blobs.append(blob)

            if safe_blobs:
                  self.blobs = safe_blobs
                        
            return self.blobs

      def get_files_page(self, page, files):
            result, error = self.run_command("./lbrynet file list --page_size=100 --page=" + str(page))

            if error:
                  files.append({"error": error, "page": page})

            for f in result["items"]:
                  files.append(f)

            if result["page"] < result["total_pages"]:
                  return self.get_files_page(page + 1, files)

            return files

      def get_files(self):
            files = self.get_files_page(1, [])

            safe_files = []

            for f in files:
                  if "error" in f:
                        print("Error")
                        print(f["page"])
                        print(f["error"])
                  else:
                        safe_files.append(f)

            if safe_files:
                  self.files = safe_files

            return self.files

      def announce_all(self):
            errors = []
            successes = []
            
            print("announce " + str(len(self.files)) + " files")
            
            if len(self.files) > 0:
                  for f in self.files:
                        result, error = self.run_command("./lbrynet blob announce --stream_hash=\"" + f["stream_hash"] + "\"")

                        if error:
                              errors.append(error)
                        else:
                              successes.append(result)

            
            print("\t" + str(len(successes)) + " successfuly announces")
            print("\t" + str(len(errors)) + " failed announces")
            

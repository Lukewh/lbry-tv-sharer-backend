import requests
from zipfile import ZipFile
from urllib.request import urlretrieve
import os
from stat import *
import time

def download_sdk():
    downloaded_version = None
    
    if not os.path.exists("lbrynet"):
        print("SDK doesn't exist")
    else:
        with open("sdk_version", "r") as f:
            downloaded_version = f.readlines()[0]

    last_updated = None
    if downloaded_version:
        last_updated = int(os.path.getmtime("sdk_version"))

    if not last_updated or int(time.time()) - last_updated > 60 * 60:
        try:
            response = requests.get("https://api.github.com/repos/lbryio/lbry-sdk/releases/latest")

            VERSION = response.json().get("tag_name")
        except Exception as error:
            print(error)

        if not downloaded_version or downloaded_version != VERSION:
            print("Getting version " + VERSION)
        
            urlretrieve(
                "https://github.com/lbryio/lbry-sdk/releases/download/" + VERSION + "/lbrynet-linux.zip",
                "lbrynet-linux.zip"
            )

            print("Done")

            print("Extracting")

            with ZipFile("lbrynet-linux.zip", "r") as zip:
                zip.extractall(".")

            print("Done")

            print("Making executable")

            st = os.stat("lbrynet")
            os.chmod("lbrynet", st.st_mode | S_IEXEC)

            with open("sdk_version", "w") as f:
                f.write(VERSION)

            print("All done")
        else:
            print("SDK up to date")

if __name__ == "__main__":
     download_sdk()

     

document.addEventListener("DOMContentLoaded", function () {
  const backendInput = document.getElementById("backend_url");

  chrome.storage.sync.get("url", function(data) {
    chrome.extension.getBackgroundPage().console.log(data);
    if (data.url) {
      backendInput.value = data.url;
    }
  });

  backendInput.addEventListener("keyup", function(e) {
    chrome.storage.sync.set({url: this.value});
  });
});

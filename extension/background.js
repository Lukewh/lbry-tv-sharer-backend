chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  if (changeInfo.url) {
    if (changeInfo.url.indexOf("https://lbry.tv/@") === 0) {
      chrome.tabs.sendMessage(tabId, {text: "uri_please"}, function(uri) {
        if(uri) {
          chrome.storage.sync.get("url", function(data) {
            if (data.url) {
              console.log(`${data.url}/get`);
              fetch(`${data.url}/get`, {
                method: "POST",
                mode: "cors",
                headers: {
                  "Content-Type": "application/json"
                },
                body: JSON.stringify({
                  uri: uri
                })
              }).then(response => response.json())
                .then(data => {
                  console.log(data);
                });
            }
          });
        }
      });
    }
  }
});

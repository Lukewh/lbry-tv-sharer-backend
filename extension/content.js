chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse){
  if (msg.text === "uri_please") {
    const btn = document.querySelector("button[aria-label^='lbry://@']");

    sendResponse(btn.getAttribute("aria-label"));
  }
});

import schedule
import time

from modules.lbry import Lbry

lbry_client = Lbry()

blobs = []

def job():
    lbry_client.get_files()
    lbry_client.announce_all()

job()
schedule.every(30).minutes.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)

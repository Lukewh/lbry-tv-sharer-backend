import flask
from flask_cors import CORS, cross_origin
from modules.download_sdk import download_sdk
from modules.lbry import Lbry

download_sdk()

lbry_client = Lbry()

print(lbry_client.stop_daemon())
print(lbry_client.start_daemon())

app = flask.Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"

@app.route("/get", methods=["POST"])
@cross_origin()
def get_uri():
    data = flask.request.get_json()
    
    if data and "uri" in data:
        if not lbry_client.is_running():
            return "Lbry Deamon not running"
        else:
            result = lbry_client.get_uri(data["uri"])
            return flask.jsonify(result)
    else:
        return "Nope"


@app.route("/running")
@cross_origin()
def is_running():
    return flask.jsonify({"running": lbry_client.is_running()})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001)
